
package my.blackjack;

import java.awt.Color;
import static java.awt.Color.black;
import static java.awt.Color.red;

public class Card {
    //Numeric value of the card 1-10. Ace logic to be done in Main class
    public int value;
    //ASCII value of the card - Using Playing Cards font
    public int asciiValue;
    public Color color;

    //Constructor loads the Suit and Value from the Playing Card font.
    //ASCII values are:
    //Diamonds 65 - 77
    //Hearts 78 - 90
    //Spades 97 - 109
    //Clubs 110 - 122
    public Card(int s, int v){

        switch (s){
            case 1:
                asciiValue = 64;
                color = red;
                break;
            case 2:
                asciiValue = 77;
                color = red;
                break;
            case 3:
                asciiValue = 96;
                color = black;
                break;
            case 4:
                asciiValue = 109;
                color = black;
                break;
        }

        switch (v){
            case 1:
                asciiValue += 1; //Ace
                value = 1;
                break;
            case 2:
                asciiValue += 2;
                value = 2;
                break;
            case 3:
                asciiValue += 3;
                value = 3;
                break;
            case 4:
                asciiValue += 4;
                value = 4;
                break;
            case 5:
                asciiValue += 5;
                value = 5;
                break;
            case 6:
                asciiValue += 6;
                value = 6;
                break;
            case 7:
                asciiValue += 7;
                value = 7;
                break;
            case 8:
                asciiValue += 8;
                value = 8;
                break;
            case 9:
                asciiValue += 9;
                value = 9;
                break;
            case 10:
                asciiValue += 10; 
                value = 10;
                break;
            case 11:
                asciiValue += 11; //Jack
                value = 10;
                break;
            case 12:
                asciiValue += 12; //Queen
                value = 10;
                break;
            case 13:
                asciiValue += 13; //King
                value = 10;
                break;
        }
    }
}
