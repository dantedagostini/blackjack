
package my.blackjack;

public class Deck {
    public Card[] deck;

    //Constructor builds the Deck based off the ASCII values in the Card Class
    public Deck(){
        deck = new Card[52];
        int cardCount = 0; //Card count starts at 0

        //Outer loop goes through the Suits. 4 in total
        for (int suit = 1; suit <= 4; suit++ ){
            //Inner loop goes through Cards. 13 in each Suit
            for (int value = 1; value <= 13; value++){
                deck[cardCount] = new Card(suit,value);
                cardCount++;
            }
        }
    }

    //Shuffling the deck(Shuffle the index of the array) - http://technojeeves.com/index.php/9-freebies/59-shuffle-array-in-java
    public void shuffle() {
        for (int i = deck.length - 1; i > 0; i--) {
            int rand = (int) (Math.random() * (i + 1));
            Card temp = deck[i];
            deck[i] = deck[rand];
            deck[rand] = temp;
        }
    }
}
