
package my.blackjack;

import java.util.ArrayList;

public class Game {
    
    //New Deck object containing an array of Card objects.
    Deck newDeck = new Deck();

    //Array list for Player and Dealer, holding the cards in their hand
    ArrayList<Card> playerHand = new ArrayList<>();
    ArrayList<Card> dealerHand = new ArrayList<>();

    //Player and Dealer totals, used to determine the winner
    int playerTotal = 0;
    int dealerTotal = 0;
    //used to determine which card to give the player; 3rd, 4th, etc
    int hitNumber = 0;
    //used to determine which card to deal
    int cardNumber = 0;
  
        
   public void newGame() {

        //Shuffling the Deck
        newDeck.shuffle();

        //Dealing the initial 4 cards
        playerHand.add(newDeck.deck[0]);
        dealerHand.add(newDeck.deck[1]);
        playerHand.add(newDeck.deck[2]);
        dealerHand.add(newDeck.deck[3]);
        cardNumber = 4; //4 will be the next card dealt, which is the 5th element in the deck array

        //Initial totals of the two cards per player
        dealerTotal = dealerHand.get(0).value; //Second card has yet to be revealed to the player
        
        playerTotal = playerHand.get(0).value + playerHand.get(1).value;

    }
   
   //Gives out a new card 
   public void dealCard(ArrayList<Card> hand, int i){
        hand.add(newDeck.deck[i]);
        ++cardNumber;
   }

   // Gives the proper Ace Value for Player
   public void setPlayerAceValue()
   {
       int aceQuantity = 0;
       for(int i = 0; i<playerHand.size(); i++)
        {
            if(playerHand.get(i).value == 1)
            {
                aceQuantity+=1;
            }
        }
           if(this.playerTotal <= 11 && aceQuantity > 0)
           {
              this.playerTotal += 10;
           }
           
           else if (this.playerTotal > 21 && aceQuantity >0)
           {    
               this.playerTotal -= 10;
           }

   }
   
   // Gives the proper Ace Value for Dealer
   public void setDealerAceValue()
   {
       int aceQuantity = 0;
       for(int i = 0; i<dealerHand.size(); i++)
        {
            if(dealerHand.get(i).value == 1)
            {
                aceQuantity+=1;
            }
        }
        if(this.dealerTotal <= 11 && aceQuantity > 0)
           {
              this.dealerTotal += 10;
           }
           
           else if (this.dealerTotal > 21 && aceQuantity >0)
           {    
               this.dealerTotal -= 10;
           }
   }    
   
   //Getters and Setters below
    
    public ArrayList<Card> getPlayerHand() {
        return playerHand;
    }

    public void setPlayerHand(ArrayList<Card> playerHand) {
        this.playerHand = playerHand;
    }

    public ArrayList<Card> getDealerHand() {
        return dealerHand;
    }

    public void setDealerHand(ArrayList<Card> dealerHand) {
        this.dealerHand = dealerHand;
    }

    public int getPlayerTotal() {
        return playerTotal;
    }

    public void setPlayerTotal(int playerTotal) {
        this.playerTotal = playerTotal;
    }

    public int getDealerTotal() {
        return dealerTotal;
    }

    public void setDealerTotal(int dealerTotal) {
        this.dealerTotal = dealerTotal;
    }
}
